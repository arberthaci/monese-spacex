package al.monese.spacex.data.prefs

import al.monese.spacex.di.PreferenceInfo
import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

class AppPreferenceHelper @Inject constructor(context: Context, @PreferenceInfo private val prefFileName: String) : PreferenceHelper {

    companion object {
        private const val PREF_KEY_IS_FIRST_OPEN = "PREF_KEY_IS_FIRST_OPEN"
    }

    private val mPrefs: SharedPreferences = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE)

    override fun getIsFirstOpen(): Boolean = mPrefs.getBoolean(PREF_KEY_IS_FIRST_OPEN, true)

    override fun setIsFirstOpen(isFirstOpen: Boolean) = mPrefs.edit { putBoolean(PREF_KEY_IS_FIRST_OPEN, isFirstOpen) }
}