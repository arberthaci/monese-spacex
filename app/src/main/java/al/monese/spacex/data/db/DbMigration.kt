package al.monese.spacex.data.db

import io.realm.DynamicRealm
import io.realm.RealmMigration

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

class DbMigration : RealmMigration {

    override fun migrate(realm: DynamicRealm, oldVersion: Long, newVersion: Long) {
        //var oldVersion = oldVersion
        //val schema = realm.schema
    }


    override fun hashCode(): Int {
        return 37
    }

    override fun equals(other: Any?): Boolean {
        return other is DbMigration
    }
}