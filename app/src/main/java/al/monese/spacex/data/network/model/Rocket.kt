package al.monese.spacex.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

open class Rocket(@PrimaryKey
                  @Expose
                  @SerializedName("id")
                  var id: Int? = null,

                  @Expose
                  @SerializedName("active")
                  var active: Boolean? = null,

                  @Expose
                  @SerializedName("country")
                  var country: String? = null,

                  @Expose
                  @SerializedName("engines")
                  var engines: Engine? = null,

                  @Expose
                  @SerializedName("description")
                  var description: String? = null,

                  @Expose
                  @SerializedName("rocket_id")
                  var rocketId: String? = null,

                  @Expose
                  @SerializedName("rocket_name")
                  var rocketName: String? = null,

                  @Expose
                  @SerializedName("rocket_type")
                  var rocketType: String? = null): RealmObject()