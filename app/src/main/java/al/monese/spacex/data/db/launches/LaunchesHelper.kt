package al.monese.spacex.data.db.launches

import al.monese.spacex.data.network.model.Launch

/**
 * Created by Arbër Thaçi on 18-12-24.
 * Email: arberlthaci@gmail.com
 */

interface LaunchesHelper {

    fun insertLaunches(launches: List<Launch>)

    fun retrieveAllLaunchesByRocketId(rocketId: String): List<Launch>
}