package al.monese.spacex.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

open class Engine(@Expose
                  @SerializedName("number")
                  var number: Int? = null,

                  @Expose
                  @SerializedName("type")
                  var type: String? = null): RealmObject()