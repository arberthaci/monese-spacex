package al.monese.spacex.data.prefs

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

interface PreferenceHelper {

    fun getIsFirstOpen(): Boolean

    fun setIsFirstOpen(isFirstOpen: Boolean)
}