package al.monese.spacex.data.network

import al.monese.spacex.data.network.model.Rocket
import io.reactivex.Observable
import retrofit2.http.GET

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

interface RocketsApi {

    @GET(ApiEndPoint.ENDPOINT_ROCKETS)
    fun getAllRockets(): Observable<List<Rocket>>
}