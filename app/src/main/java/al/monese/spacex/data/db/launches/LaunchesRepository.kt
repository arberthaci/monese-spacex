package al.monese.spacex.data.db.launches

import al.monese.spacex.data.network.model.Launch
import io.realm.Realm
import io.realm.kotlin.where
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 18-12-24.
 * Email: arberlthaci@gmail.com
 */

class LaunchesRepository @Inject constructor(private val realm: Realm): LaunchesHelper {

    override fun insertLaunches(launches: List<Launch>) {
        Realm.getDefaultInstance().use { realm ->
            realm.executeTransaction { mRealm ->
                mRealm.insertOrUpdate(launches)
            }
        }
    }

    override fun retrieveAllLaunchesByRocketId(rocketId: String): List<Launch> {
        return Realm.getDefaultInstance().use { realm ->
            realm.copyFromRealm(realm.where<Launch>()
                    .equalTo("rocketId", rocketId)
                    .findAll())
        }
    }
}