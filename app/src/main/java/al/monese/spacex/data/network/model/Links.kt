package al.monese.spacex.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

/**
 * Created by Arbër Thaçi on 18-12-24.
 * Email: arberlthaci@gmail.com
 */

open class Links(@Expose
                 @SerializedName("mission_patch_small")
                 var missionPatchSmall: String? = null): RealmObject()