package al.monese.spacex.data.db

import al.monese.spacex.data.db.launches.LaunchesHelper
import al.monese.spacex.data.db.rockets.RocketsHelper
import al.monese.spacex.data.network.model.Launch
import al.monese.spacex.data.network.model.Rocket
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

class DbOperations @Inject internal constructor(private var rocketsHelper: RocketsHelper, private var launchesHelper: LaunchesHelper)
    : RocketsHelper, LaunchesHelper {

    /**
     **********************************
     * RocketsHelper implementation
     * Start
     **********************************
     **/

    override fun insertRockets(rockets: List<Rocket>) =
            rocketsHelper.insertRockets(rockets = rockets)

    override fun retrieveAllRockets(): List<Rocket> =
            rocketsHelper.retrieveAllRockets()

    /**
     **********************************
     * RocketsHelper implementation
     * End
     **********************************
     **/

    /**
     **********************************
     * LaunchesHelper implementation
     * Start
     **********************************
     **/

    override fun insertLaunches(launches: List<Launch>) =
            launchesHelper.insertLaunches(launches = launches)

    override fun retrieveAllLaunchesByRocketId(rocketId: String): List<Launch> =
            launchesHelper.retrieveAllLaunchesByRocketId(rocketId = rocketId)

    /**
     **********************************
     * LaunchesHelper implementation
     * End
     **********************************
     **/
}