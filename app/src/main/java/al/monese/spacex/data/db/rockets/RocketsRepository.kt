package al.monese.spacex.data.db.rockets

import al.monese.spacex.data.network.model.Rocket
import io.realm.Realm
import io.realm.kotlin.where
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

class RocketsRepository @Inject constructor(private val realm: Realm): RocketsHelper {

    override fun insertRockets(rockets: List<Rocket>) {
        Realm.getDefaultInstance().use { realm ->
            realm.executeTransaction { mRealm ->
                mRealm.insertOrUpdate(rockets)
            }
        }
    }

    override fun retrieveAllRockets(): List<Rocket> {
        return Realm.getDefaultInstance().use { realm ->
            realm.copyFromRealm(realm.where<Rocket>()
                    .findAll())
        }
    }
}