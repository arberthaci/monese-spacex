package al.monese.spacex.data.network

import al.monese.spacex.BuildConfig

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

object ApiEndPoint {

    const val ENDPOINT_ROCKETS = BuildConfig.BASE_URL + "rockets/"
    const val ENDPOINT_LAUNCHES = BuildConfig.BASE_URL + "launches/"
}