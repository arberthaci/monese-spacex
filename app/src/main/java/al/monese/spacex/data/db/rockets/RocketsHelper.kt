package al.monese.spacex.data.db.rockets

import al.monese.spacex.data.network.model.Rocket

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

interface RocketsHelper {

    fun insertRockets(rockets: List<Rocket>)

    fun retrieveAllRockets(): List<Rocket>
}