package al.monese.spacex.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

/**
 * Created by Arbër Thaçi on 18-12-24.
 * Email: arberlthaci@gmail.com
 */

open class Launch(@PrimaryKey
                  @Expose
                  @SerializedName("mission_name")
                  var missionName: String? = null,

                  @Expose
                  @SerializedName("launch_year")
                  var launchYear: String? = null,

                  @Expose
                  @SerializedName("launch_date_local")
                  var launchDateLocal: Date? = null,

                  @Expose
                  @SerializedName("launch_success")
                  var launchSuccess: Boolean? = null,

                  @Expose
                  @SerializedName("links")
                  var links: Links? = null,

                  var rocketId: String? = null): RealmObject()