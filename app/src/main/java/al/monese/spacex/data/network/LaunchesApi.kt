package al.monese.spacex.data.network

import al.monese.spacex.data.network.model.Launch
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.QueryMap

/**
 * Created by Arbër Thaçi on 18-12-24.
 * Email: arberlthaci@gmail.com
 */

interface LaunchesApi {

    @GET(ApiEndPoint.ENDPOINT_LAUNCHES)
    fun getAllLaunchesByRocketId(@QueryMap filterParameters: Map<String, String>?): Observable<List<Launch>>
}