package al.monese.spacex.ui.base.interactor

import al.monese.spacex.data.db.DbOperations
import al.monese.spacex.data.network.ApiHelper
import al.monese.spacex.data.prefs.PreferenceHelper

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

open class BaseInteractor() : MVPInteractor {

    protected lateinit var preferenceHelper: PreferenceHelper
    protected lateinit var apiHelper: ApiHelper
    protected lateinit var dbOperations: DbOperations

    constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper, dbOperations: DbOperations) : this() {
        this.preferenceHelper = preferenceHelper
        this.apiHelper = apiHelper
        this.dbOperations = dbOperations
    }
}