package al.monese.spacex.ui.rocketdetails.interactor

import al.monese.spacex.data.network.model.Launch
import al.monese.spacex.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

interface RocketDetailsMVPInteractor : MVPInteractor {

    fun retrieveLaunchesByRocketIdFromServer(rocketId: String): Observable<List<Launch>>

    fun retrieveLaunchesByRocketIdFromRealm(rocketId: String): List<Launch>
    fun insertOrUpdateLaunchesInRealm(launches: List<Launch>)
}