package al.monese.spacex.ui.rockets

import al.monese.spacex.ui.rockets.view.RocketsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

@Module
internal abstract class RocketsFragmentProvider {

    @ContributesAndroidInjector(modules = [RocketsFragmentModule::class])
    internal abstract fun provideRocketsFragmentFactory(): RocketsFragment
}