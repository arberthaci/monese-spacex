package al.monese.spacex.ui.rockets.presenter

import al.monese.spacex.data.network.model.Rocket
import al.monese.spacex.ui.base.presenter.BasePresenter
import al.monese.spacex.ui.rockets.interactor.RocketsMVPInteractor
import al.monese.spacex.ui.rockets.view.RocketsMVPView
import al.monese.spacex.utils.customviews.ErrorViewState
import al.monese.spacex.utils.rx.RetryAfterNoNetworkConnectionWithDelay
import al.monese.spacex.utils.rx.RxUtils
import al.monese.spacex.utils.rx.SchedulerProvider
import al.monese.spacex.utils.rx.exceptions.NoNetworkConnectionException
import al.monese.spacex.utils.rx.exceptions.NoResultsException
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.exceptions.Exceptions
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

class RocketsPresenter <V : RocketsMVPView, I : RocketsMVPInteractor> @Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable), RocketsMVPPresenter<V, I> {

    private var mAllRockets: List<Rocket> = arrayListOf()
    private var mFilteredRockets: MutableList<Rocket> = arrayListOf()

    override fun onViewPrepared() {
        retrieveDataFromServer()
    }

    override fun onSwipeToRefreshOccurred() {
        retrieveDataFromServer(swipe = true)
    }

    override fun onFilterRocketsSelected() {
        getView()?.openFilterRocketsDialog()
    }

    override fun onFilterRocketsApplied(filterIndex: Int) {
        when (filterIndex) {
            0 -> getView()?.displayRockets(mAllRockets)
            1 -> {
                mFilteredRockets.clear()
                mAllRockets.forEach {
                    if (it.active == true)
                        mFilteredRockets.add(it)
                }
                getView()?.displayRockets(mFilteredRockets)
            }
            2 -> {
                mFilteredRockets.clear()
                mAllRockets.forEach {
                    if (it.active != true)
                        mFilteredRockets.add(it)
                }
                getView()?.displayRockets(mFilteredRockets)
            }
        }
    }

    private fun retrieveDataFromServer(swipe: Boolean = false) {
        getView()?.let {
            if (!swipe)
                it.prepareLoading()

            compositeDisposable.add(it.isNetworkConnected()
                    .switchMap { isConnected ->
                        if (isConnected) {
                            interactor!!.retrieveRocketsFromServer()
                                    .map { rockets -> RxUtils.checkApiResponseData(rockets) }
                        }
                        else throw Exceptions.propagate(NoNetworkConnectionException())
                    }
                    .retryWhen(RetryAfterNoNetworkConnectionWithDelay(3, 2))
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe (
                            { rockets ->
                                mAllRockets = rockets
                                interactor?.insertOrUpdateRocketsInRealm(rockets = mAllRockets)
                                it.displayRockets(mAllRockets)
                            },
                            { err ->
                                mAllRockets = interactor?.retrieveRocketsFromRealm() ?: arrayListOf()
                                if (mAllRockets.isNotEmpty())
                                    it.displayRockets(mAllRockets)
                                else {
                                    when (err) {
                                        is NoNetworkConnectionException -> {
                                            it.showErrorView(ErrorViewState.NO_CONNECTION)
                                        }
                                        is NoResultsException -> {
                                            it.showErrorView(ErrorViewState.NO_RESULTS)
                                        }
                                        else -> {
                                            it.showErrorView(ErrorViewState.NO_RESPONSE)
                                        }
                                    }
                                }
                            }
                    )
            )
        }
    }
}