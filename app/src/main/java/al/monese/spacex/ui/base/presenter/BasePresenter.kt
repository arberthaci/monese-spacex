package al.monese.spacex.ui.base.presenter

import al.monese.spacex.ui.base.interactor.MVPInteractor
import al.monese.spacex.ui.base.view.MVPView
import al.monese.spacex.utils.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

abstract class BasePresenter<V : MVPView, I : MVPInteractor> internal constructor(protected var interactor: I?, protected val schedulerProvider: SchedulerProvider, protected val compositeDisposable: CompositeDisposable) : MVPPresenter<V, I> {

    private var view: V? = null
    private val isViewAttached: Boolean get() = view != null

    override fun onAttach(view: V?) {
        this.view = view
    }

    override fun getView(): V? = view

    override fun onDetach() {
        compositeDisposable.dispose()
        view = null
        interactor = null
    }
}