package al.monese.spacex.ui.rocketdetails.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import al.monese.spacex.R
import al.monese.spacex.data.network.model.Launch
import al.monese.spacex.data.network.model.Rocket
import al.monese.spacex.ui.base.view.BaseFragment
import al.monese.spacex.ui.rocketdetails.interactor.RocketDetailsMVPInteractor
import al.monese.spacex.ui.rocketdetails.presenter.RocketDetailsMVPPresenter
import al.monese.spacex.utils.YearValueFormatter
import al.monese.spacex.utils.customviews.ErrorViewState
import al.monese.spacex.utils.extensions.hide
import al.monese.spacex.utils.extensions.show
import al.monese.spacex.utils.rvitemdecorations.DividerItemDecoration
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import kotlinx.android.synthetic.main.fragment_rocket_details.*
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

class RocketDetailsFragment : BaseFragment(), RocketDetailsMVPView {

    @Inject
    internal lateinit var presenter: RocketDetailsMVPPresenter<RocketDetailsMVPView, RocketDetailsMVPInteractor>
    @Inject
    internal lateinit var launchAdapter: LaunchAdapter
    @Inject
    internal lateinit var layoutManager: LinearLayoutManager

    lateinit var rocket: Rocket

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_rocket_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.onAttach(this)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        presenter.onDetach()
        super.onDestroyView()
    }

    /**
     **********************************
     * BaseFragment implementation
     * Start
     **********************************
     **/

    override fun setUp() {
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        rv_launches.layoutManager = layoutManager
        rv_launches.itemAnimator = DefaultItemAnimator()
        val divider = ContextCompat.getDrawable(context!!, R.drawable.divider)
        divider?.let { rv_launches.addItemDecoration(DividerItemDecoration(it)) }
        rv_launches.setHasFixedSize(false)
        rv_launches.adapter = launchAdapter

        presenter.onViewPrepared(rocket = rocket)
    }

    /**
     **********************************
     * BaseFragment implementation
     * End
     **********************************
     **/

    /**
     **********************************
     * RocketDetailsMVPView implementation
     * Start
     **********************************
     **/

    override fun prepareLoading() {
        showLoadingIndicator()
        ev_rocket_details.setState(ErrorViewState.NO_ERROR)
        ll_rocket_details.hide()
    }

    override fun setEntriesToChart(entries: List<Entry>) {
        val dataSet = LineDataSet(entries, getString(R.string.rocket_details_chart_number_of_launches_label))
        dataSet.color = ContextCompat.getColor(context!!, R.color.colorAccent)
        dataSet.valueTextColor = ContextCompat.getColor(context!!, R.color.grey_700)
        dataSet.valueTextSize = 10f

        lc_launches_by_year.data = LineData(dataSet)
        val description = Description()
        description.text = ""
        lc_launches_by_year.description = description

        val xAxis = lc_launches_by_year.xAxis
        xAxis.valueFormatter = YearValueFormatter()

        lc_launches_by_year.invalidate()
    }

    override fun setRocketDescription(description: String?) {
        description?.let { tv_rocket_details_description_value.text = it }
    }

    override fun setLaunchesList(launches: List<Launch>?): Unit? = launches?.let {
        launchAdapter.presenter.insertLaunchesToList(it)
        tv_rocket_details_no_launches.hide()
        rv_launches.show()
    }

    override fun displayDetails() {
        ev_rocket_details.setState(ErrorViewState.NO_ERROR)
        ll_rocket_details.show()
        dismissLoadingIndicator()
    }

    override fun showErrorView(state: ErrorViewState) {
        ll_rocket_details.hide()
        ev_rocket_details.setState(state)
        dismissLoadingIndicator()
    }

    /**
     **********************************
     * RocketDetailsMVPView implementation
     * End
     **********************************
     **/

    companion object {
        internal val TAG = "rocket_details_ft"
        internal val TITLE = "Rocket Details"

        fun newInstance(): RocketDetailsFragment {
            return RocketDetailsFragment()
        }
    }
}
