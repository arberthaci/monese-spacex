package al.monese.spacex.ui.base.view

import al.monese.spacex.utils.AppConstants
import io.reactivex.Observable

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

interface MVPView {

    fun showLoadingIndicator()

    fun dismissLoadingIndicator()

    fun showAlerter(alerterMode: AppConstants.AlerterMode, message: String)

    fun isNetworkConnected(): Observable<Boolean>

    fun hasNetworkConnection(): Boolean
}