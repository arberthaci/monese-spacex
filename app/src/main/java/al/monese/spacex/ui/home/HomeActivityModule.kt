package al.monese.spacex.ui.home

import al.monese.spacex.ui.home.interactor.HomeInteractor
import al.monese.spacex.ui.home.interactor.HomeMVPInteractor
import al.monese.spacex.ui.home.presenter.HomeMVPPresenter
import al.monese.spacex.ui.home.presenter.HomePresenter
import al.monese.spacex.ui.home.view.HomeMVPView
import dagger.Module
import dagger.Provides

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

@Module
class HomeActivityModule {

    @Provides
    internal fun provideHomeInteractor(homeInteractor: HomeInteractor): HomeMVPInteractor = homeInteractor

    @Provides
    internal fun provideHomePresenter(homePresenter: HomePresenter<HomeMVPView, HomeMVPInteractor>)
            : HomeMVPPresenter<HomeMVPView, HomeMVPInteractor> = homePresenter
}