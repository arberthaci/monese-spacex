package al.monese.spacex.ui.rockets.view

import al.monese.spacex.data.network.model.Rocket
import al.monese.spacex.ui.base.view.MVPView
import al.monese.spacex.utils.customviews.ErrorViewState

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

interface RocketsMVPView : MVPView {

    fun prepareLoading()
    fun displayRockets(rockets: List<Rocket>?): Unit?
    fun showErrorView(state: ErrorViewState)

    fun openFilterRocketsDialog()
}