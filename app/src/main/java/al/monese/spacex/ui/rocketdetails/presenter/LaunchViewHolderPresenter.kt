package al.monese.spacex.ui.rocketdetails.presenter

import al.monese.spacex.ui.rocketdetails.view.LaunchViewHolderView
import org.joda.time.DateTime

/**
 * Created by Arbër Thaçi on 18-12-24.
 * Email: arberlthaci@gmail.com
 */

class LaunchViewHolderPresenter (private var launchAdapterPresenter: LaunchAdapterPresenter) {

    fun clearViewHolder(viewHolderView: LaunchViewHolderView) = viewHolderView.let {
        it.setMissionName()
        it.setLaunchDate()
        it.setIsSuccessful()
        it.loadThumbnail()
        it.setLaunchYear()
        it.showHeader(false)
    }

    fun onBindViewHolder(position: Int, viewHolderView: LaunchViewHolderView) = viewHolderView.let {
        val launch = launchAdapterPresenter.getItemAt(position)

        it.setMissionName(launch.missionName)
        it.setLaunchDate(DateTime(launch.launchDateLocal))
        it.setIsSuccessful(launch.launchSuccess)
        it.loadThumbnail(launch.links?.missionPatchSmall)
        it.setLaunchYear(launch.launchYear)

        val launchYear = launch.launchYear ?: ""
        when {
            launchAdapterPresenter.getItemCount() == 1 -> it.showHeader()
            position == 0 -> it.showHeader()
            launchYear != launchAdapterPresenter.getItemAt(position - 1).launchYear -> it.showHeader()
            else -> it.showHeader(false)
        }
    }
}