package al.monese.spacex.ui.rocketdetails.presenter

import al.monese.spacex.data.network.model.Launch
import al.monese.spacex.ui.rocketdetails.view.LaunchAdapterView
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 18-12-24.
 * Email: arberlthaci@gmail.com
 */

class LaunchAdapterPresenter @Inject internal constructor(private val launchListItems: MutableList<Launch>) {

    var view: LaunchAdapterView? = null

    fun getItemCount() =
            this.launchListItems.size

    fun getItemAt(position: Int) =
            this.launchListItems[position]

    fun insertLaunchesToList(launches: List<Launch>) {
        clearList()
        loadMoreLaunchesToList(launches)
    }

    fun loadMoreLaunchesToList(launches: List<Launch>) {
        this.launchListItems.addAll(launches)
        notifyDataSetChanged()
    }

    fun clearList() {
        this.launchListItems.clear()
        notifyDataSetChanged()
    }

    fun notifyDataSetChanged() =
            view?.adapterNotifyDataSetChanged()
}