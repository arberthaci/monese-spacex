package al.monese.spacex.ui.rockets.interactor

import al.monese.spacex.data.network.model.Rocket
import al.monese.spacex.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

interface RocketsMVPInteractor : MVPInteractor {

    fun retrieveRocketsFromServer(): Observable<List<Rocket>>

    fun retrieveRocketsFromRealm(): List<Rocket>
    fun insertOrUpdateRocketsInRealm(rockets: List<Rocket>)
}