package al.monese.spacex.ui.rockets.presenter

import al.monese.spacex.data.network.model.Rocket
import al.monese.spacex.ui.rockets.view.RocketAdapterView
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

class RocketAdapterPresenter @Inject internal constructor(private val rocketListItems: MutableList<Rocket>) {

    var view: RocketAdapterView? = null

    fun getItemCount() =
            this.rocketListItems.size

    fun getItemAt(position: Int) =
            this.rocketListItems[position]

    fun insertRocketsToList(rockets: List<Rocket>) {
        clearList()
        loadMoreRocketsToList(rockets)
    }

    fun loadMoreRocketsToList(rockets: List<Rocket>) {
        this.rocketListItems.addAll(rockets)
        notifyDataSetChanged()
    }

    fun clearList() {
        this.rocketListItems.clear()
        notifyDataSetChanged()
    }

    fun notifyDataSetChanged() =
            view?.adapterNotifyDataSetChanged()

    fun itemClickedAtPosition(position: Int) =
            view?.adapterItemClicked(getItemAt(position))
}