package al.monese.spacex.ui.rocketdetails.presenter

import al.monese.spacex.data.network.model.Launch
import al.monese.spacex.data.network.model.Rocket
import al.monese.spacex.ui.base.presenter.BasePresenter
import al.monese.spacex.ui.rocketdetails.interactor.RocketDetailsMVPInteractor
import al.monese.spacex.ui.rocketdetails.view.RocketDetailsMVPView
import al.monese.spacex.utils.customviews.ErrorViewState
import al.monese.spacex.utils.rx.RetryAfterNoNetworkConnectionWithDelay
import al.monese.spacex.utils.rx.RxUtils
import al.monese.spacex.utils.rx.SchedulerProvider
import al.monese.spacex.utils.rx.exceptions.NoNetworkConnectionException
import al.monese.spacex.utils.rx.exceptions.NoResultsException
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.utils.EntryXComparator
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.exceptions.Exceptions
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

class RocketDetailsPresenter <V : RocketDetailsMVPView, I : RocketDetailsMVPInteractor> @Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable), RocketDetailsMVPPresenter<V, I> {

    private var mAllLaunches: List<Launch> = arrayListOf()

    override fun onViewPrepared(rocket: Rocket) {
        getView()?.let {
            it.prepareLoading()
            it.setRocketDescription(rocket.description)

            compositeDisposable.add(it.isNetworkConnected()
                    .switchMap { isConnected ->
                        if (isConnected) {
                            interactor!!.retrieveLaunchesByRocketIdFromServer(rocketId = rocket.rocketId ?: "")
                                    .map { launches -> RxUtils.checkApiResponseData(launches) }
                        }
                        else throw Exceptions.propagate(NoNetworkConnectionException())
                    }
                    .retryWhen(RetryAfterNoNetworkConnectionWithDelay(3, 2))
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe (
                            { launches ->
                                mAllLaunches = launches

                                rocket.rocketId?.let { mRocketId -> attachRocketIdToLaunches(rocketId = mRocketId) }
                                interactor?.insertOrUpdateLaunchesInRealm(launches = mAllLaunches)

                                displayData()
                            },
                            { err ->
                                mAllLaunches = interactor?.retrieveLaunchesByRocketIdFromRealm(rocketId = rocket.rocketId ?: "") ?: arrayListOf()
                                if (mAllLaunches.isNotEmpty()) {
                                    displayData()
                                }
                                else {
                                    when (err) {
                                        is NoNetworkConnectionException -> {
                                            it.showErrorView(ErrorViewState.NO_CONNECTION)
                                        }
                                        is NoResultsException -> {
                                            it.displayDetails()
                                        }
                                        else -> {
                                            it.showErrorView(ErrorViewState.NO_RESPONSE)
                                        }
                                    }
                                }
                            }
                    )
            )
        }
    }

    private fun attachRocketIdToLaunches(rocketId: String) {
        mAllLaunches.forEach {
            it.rocketId = rocketId
        }
    }

    private fun prepareDataForChart(): List<Entry> {
        val launchesByYear: HashMap<String, Int> = hashMapOf()
        mAllLaunches.forEach { mLaunch ->
            mLaunch.launchYear?.let { mLaunchYear ->
                launchesByYear[mLaunchYear] = (launchesByYear[mLaunchYear] ?: 0) + 1
            }
        }

        val entries = mutableListOf<Entry>()
        launchesByYear.forEach { mLaunchesByYear ->
            entries.add(Entry(mLaunchesByYear.key.toFloat(), mLaunchesByYear.value.toFloat()))
        }
        entries.sortWith(EntryXComparator())

        return entries
    }

    private fun displayData() {
        getView()?.let {
            it.setEntriesToChart(entries = prepareDataForChart())
            it.setLaunchesList(launches = mAllLaunches)
            it.displayDetails()
        }
    }
}