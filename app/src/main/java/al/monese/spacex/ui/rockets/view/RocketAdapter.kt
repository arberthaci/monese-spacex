package al.monese.spacex.ui.rockets.view

import al.monese.spacex.R
import al.monese.spacex.data.network.model.Rocket
import al.monese.spacex.ui.rockets.presenter.RocketAdapterPresenter
import al.monese.spacex.ui.rockets.presenter.RocketViewHolderPresenter
import al.monese.spacex.utils.rvadapterlisteners.OnRocketActionListener
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_rocket_list.view.*

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

class RocketAdapter (val presenter: RocketAdapterPresenter)
    : RecyclerView.Adapter<RocketAdapter.RocketViewHolder>(), RocketAdapterView {

    init {
        presenter.view = this
    }

    lateinit var onRocketActionListener: OnRocketActionListener

    override fun getItemCount() = this.presenter.getItemCount()

    override fun onBindViewHolder(holder: RocketViewHolder, position: Int) = holder.let {
        holder.viewHolderPresenter.clearViewHolder(it)
        holder.viewHolderPresenter.onBindViewHolder(position, it)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = RocketViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_rocket_list, parent, false))

    override fun adapterNotifyDataSetChanged() {
        notifyDataSetChanged()
    }

    override fun adapterItemClicked(rocket: Rocket) =
            onRocketActionListener.onItemSelected(rocket)

    inner class RocketViewHolder(view: View) : RecyclerView.ViewHolder(view), RocketViewHolderView {

        val viewHolderPresenter = RocketViewHolderPresenter(presenter)

        override fun setName(rocketName: String?) {
            rocketName?.let { itemView.tv_rocket_name.text = it }
                    ?: run { itemView.tv_rocket_name.text = "" }
        }

        override fun setCountry(countryName: String?) {
            countryName?.let { itemView.tv_rocket_country_name.text = it }
                    ?: run { itemView.tv_rocket_country_name.text = "" }
        }

        override fun setEngineCount(count: Int?) {
            count?.let { itemView.tv_rocket_engines_count_value.text = it.toString() }
                    ?: run { itemView.tv_rocket_engines_count_value.text = "" }
        }

        override fun setOnClickListeners() {
            if (adapterPosition != RecyclerView.NO_POSITION)
                itemView.rl_single_rocket_item.setOnClickListener { viewHolderPresenter.onClickListenerItem(adapterPosition) }
        }
    }
}