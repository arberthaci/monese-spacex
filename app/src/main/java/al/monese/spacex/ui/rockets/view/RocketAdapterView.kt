package al.monese.spacex.ui.rockets.view

import al.monese.spacex.data.network.model.Rocket

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

interface RocketAdapterView {

    fun adapterNotifyDataSetChanged()
    fun adapterItemClicked(rocket: Rocket)
}