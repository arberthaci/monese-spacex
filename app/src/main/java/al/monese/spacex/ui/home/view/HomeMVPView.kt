package al.monese.spacex.ui.home.view

import al.monese.spacex.data.network.model.Rocket
import al.monese.spacex.ui.base.view.MVPView
import android.support.v4.app.Fragment
import android.view.Menu

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

interface HomeMVPView : MVPView {

    fun setToolbarTitle(title: String)

    fun getCurrentFragment(): Fragment?
    fun openRocketsFragment()
    fun openRocketDetailsFragment(rocket: Rocket)
    fun closeRocketDetailsFragment()

    fun invalidateOptionsMenuEvent(): Unit?
    fun showToolbarForRockets(menu: Menu?): Unit?

    fun showBackButton()
    fun hideBackButton()

    fun showWelcomeDialog()
}