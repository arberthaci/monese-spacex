package al.monese.spacex.ui.rockets

import al.monese.spacex.ui.rockets.interactor.RocketsInteractor
import al.monese.spacex.ui.rockets.interactor.RocketsMVPInteractor
import al.monese.spacex.ui.rockets.presenter.RocketAdapterPresenter
import al.monese.spacex.ui.rockets.presenter.RocketsMVPPresenter
import al.monese.spacex.ui.rockets.presenter.RocketsPresenter
import al.monese.spacex.ui.rockets.view.RocketAdapter
import al.monese.spacex.ui.rockets.view.RocketsFragment
import al.monese.spacex.ui.rockets.view.RocketsMVPView
import android.support.v7.widget.LinearLayoutManager
import dagger.Module
import dagger.Provides

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

@Module
class RocketsFragmentModule {

    @Provides
    internal fun provideRocketsInteractor(interactor: RocketsInteractor): RocketsMVPInteractor = interactor

    @Provides
    internal fun provideRocketsPresenter(presenter: RocketsPresenter<RocketsMVPView, RocketsMVPInteractor>)
            : RocketsMVPPresenter<RocketsMVPView, RocketsMVPInteractor> = presenter

    @Provides
    internal fun provideRocketAdapterPresenter(): RocketAdapterPresenter = RocketAdapterPresenter(arrayListOf())

    @Provides
    internal fun provideRocketAdapter(presenter: RocketAdapterPresenter): RocketAdapter = RocketAdapter(presenter)

    @Provides
    internal fun provideLinearLayoutManager(fragment: RocketsFragment): LinearLayoutManager = LinearLayoutManager(fragment.activity)
}