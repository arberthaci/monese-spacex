package al.monese.spacex.ui.rockets.presenter

import al.monese.spacex.ui.rockets.view.RocketViewHolderView

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

class RocketViewHolderPresenter (private var rocketAdapterPresenter: RocketAdapterPresenter) {

    fun clearViewHolder(viewHolderView: RocketViewHolderView) = viewHolderView.let {
        it.setName()
        it.setCountry()
        it.setEngineCount()
    }

    fun onBindViewHolder(position: Int, viewHolderView: RocketViewHolderView) = viewHolderView.let {
        val rocket = rocketAdapterPresenter.getItemAt(position)

        it.setName(rocket.rocketName)
        it.setCountry(rocket.country)
        it.setEngineCount(rocket.engines?.number)
        it.setOnClickListeners()
    }

    fun onClickListenerItem(position: Int) = rocketAdapterPresenter.itemClickedAtPosition(position)
}