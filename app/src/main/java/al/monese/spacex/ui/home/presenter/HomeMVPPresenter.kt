package al.monese.spacex.ui.home.presenter

import al.monese.spacex.ui.base.presenter.MVPPresenter
import al.monese.spacex.ui.home.interactor.HomeMVPInteractor
import al.monese.spacex.ui.home.view.HomeMVPView
import android.view.Menu

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

interface HomeMVPPresenter<V : HomeMVPView, I : HomeMVPInteractor> : MVPPresenter<V, I> {

    fun onFragmentResumed()

    fun onBackPressed(): Boolean?

    fun onInvalidateOptionsMenu(menu: Menu?)
    fun onOptionsItemFilterSelected()

    fun onWelcomeDialogClosed()
}