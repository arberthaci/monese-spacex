package al.monese.spacex.ui.home.interactor

import al.monese.spacex.data.db.DbOperations
import al.monese.spacex.data.network.ApiHelper
import al.monese.spacex.data.prefs.PreferenceHelper
import al.monese.spacex.ui.base.interactor.BaseInteractor
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

class HomeInteractor @Inject internal constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper, dbOperations: DbOperations) : BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper, dbOperations = dbOperations), HomeMVPInteractor {

    override fun retrieveIsFirstOpenFromPrefs(): Boolean =
            preferenceHelper.getIsFirstOpen()

    override fun updateIsFirstOpenInPrefs(isFirstOpen: Boolean) =
            preferenceHelper.setIsFirstOpen(isFirstOpen = isFirstOpen)
}