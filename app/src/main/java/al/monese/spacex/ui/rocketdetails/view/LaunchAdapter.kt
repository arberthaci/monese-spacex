package al.monese.spacex.ui.rocketdetails.view

import al.monese.spacex.R
import al.monese.spacex.ui.rocketdetails.presenter.LaunchAdapterPresenter
import al.monese.spacex.ui.rocketdetails.presenter.LaunchViewHolderPresenter
import al.monese.spacex.utils.AppConstants
import al.monese.spacex.utils.extensions.hide
import al.monese.spacex.utils.extensions.loadImage
import al.monese.spacex.utils.extensions.show
import al.monese.spacex.utils.extensions.toString
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_launch_list.view.*
import org.joda.time.DateTime

/**
 * Created by Arbër Thaçi on 18-12-24.
 * Email: arberlthaci@gmail.com
 */

class LaunchAdapter (val presenter: LaunchAdapterPresenter)
    : RecyclerView.Adapter<LaunchAdapter.LaunchViewHolder>(), LaunchAdapterView {

    init {
        presenter.view = this
    }

    override fun getItemCount() = this.presenter.getItemCount()

    override fun onBindViewHolder(holder: LaunchViewHolder, position: Int) = holder.let {
        holder.viewHolderPresenter.clearViewHolder(it)
        holder.viewHolderPresenter.onBindViewHolder(position, it)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = LaunchViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_launch_list, parent, false))

    override fun adapterNotifyDataSetChanged() {
        notifyDataSetChanged()
    }

    inner class LaunchViewHolder(view: View) : RecyclerView.ViewHolder(view), LaunchViewHolderView {

        val viewHolderPresenter = LaunchViewHolderPresenter(presenter)

        override fun setMissionName(missionName: String?) {
            missionName?.let { itemView.tv_launch_mission_name.text = it }
                    ?: run { itemView.tv_launch_mission_name.text = "" }
        }

        override fun setLaunchDate(datetime: DateTime?) {
            datetime?.let { itemView.tv_launch_date.text = datetime.toString(AppConstants.DateStringMode.DATE_STRING_MODE) }
                    ?: run { itemView.tv_launch_date.text = "" }
        }

        override fun setIsSuccessful(isSuccessful: Boolean?) {
            isSuccessful?.let {
                if (isSuccessful) {
                    itemView.tv_launch_successful.text = itemView.context.getString(R.string.rocket_details_successful_label)
                    itemView.tv_launch_successful.setTextColor(ContextCompat.getColor(itemView.context, R.color.green_700))
                }
                else {
                    itemView.tv_launch_successful.text = itemView.context.getString(R.string.rocket_details_failed_label)
                    itemView.tv_launch_successful.setTextColor(ContextCompat.getColor(itemView.context, R.color.red_700))
                }
            } ?: run { itemView.tv_launch_successful.text = "" }
        }

        override fun loadThumbnail(thumbnailUrl: String?) {
            thumbnailUrl?.let { itemView.iv_launch_thumbnail.loadImage(it) }
                    ?: run { itemView.iv_launch_thumbnail.setImageDrawable(null) }
        }

        override fun setLaunchYear(year: String?) {
            year?.let { itemView.tv_launch_year.text = it }
                    ?: run { itemView.tv_launch_year.text = "" }
        }

        override fun showHeader(show: Boolean) {
            if (show)
                itemView.ll_launch_year.show()
            else
                itemView.ll_launch_year.hide()
        }
    }
}