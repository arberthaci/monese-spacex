package al.monese.spacex.ui.base.presenter

import al.monese.spacex.ui.base.interactor.MVPInteractor
import al.monese.spacex.ui.base.view.MVPView

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

interface MVPPresenter<V : MVPView, I : MVPInteractor> {

    fun onAttach(view: V?)

    fun onDetach()

    fun getView(): V?
}