package al.monese.spacex.ui.rockets.view

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

interface RocketViewHolderView {

    fun setName(rocketName: String? = null)
    fun setCountry(countryName: String? = null)
    fun setEngineCount(count: Int? = null)
    fun setOnClickListeners()
}