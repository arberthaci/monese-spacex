package al.monese.spacex.ui.rocketdetails.presenter

import al.monese.spacex.data.network.model.Rocket
import al.monese.spacex.ui.base.presenter.MVPPresenter
import al.monese.spacex.ui.rocketdetails.interactor.RocketDetailsMVPInteractor
import al.monese.spacex.ui.rocketdetails.view.RocketDetailsMVPView

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

interface RocketDetailsMVPPresenter <V : RocketDetailsMVPView, I : RocketDetailsMVPInteractor> : MVPPresenter<V, I> {

    fun onViewPrepared(rocket: Rocket)
}