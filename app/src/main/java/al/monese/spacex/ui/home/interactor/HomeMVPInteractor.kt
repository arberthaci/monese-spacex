package al.monese.spacex.ui.home.interactor

import al.monese.spacex.ui.base.interactor.MVPInteractor

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

interface HomeMVPInteractor : MVPInteractor {

    fun retrieveIsFirstOpenFromPrefs(): Boolean
    fun updateIsFirstOpenInPrefs(isFirstOpen: Boolean)
}