package al.monese.spacex.ui.rocketdetails.view

import org.joda.time.DateTime

/**
 * Created by Arbër Thaçi on 18-12-24.
 * Email: arberlthaci@gmail.com
 */


interface LaunchViewHolderView {

    fun setMissionName(missionName: String? = null)
    fun setLaunchDate(datetime: DateTime? = null)
    fun setIsSuccessful(isSuccessful: Boolean? = null)
    fun loadThumbnail(thumbnailUrl: String? = null)
    fun setLaunchYear(year: String? = null)
    fun showHeader(show: Boolean = true)
}