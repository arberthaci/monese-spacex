package al.monese.spacex.ui.rocketdetails.view

import al.monese.spacex.data.network.model.Launch
import al.monese.spacex.ui.base.view.MVPView
import al.monese.spacex.utils.customviews.ErrorViewState
import com.github.mikephil.charting.data.Entry

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

interface RocketDetailsMVPView : MVPView {

    fun prepareLoading()
    fun setEntriesToChart(entries: List<Entry>)
    fun setRocketDescription(description: String?)
    fun setLaunchesList(launches: List<Launch>?): Unit?
    fun displayDetails()
    fun showErrorView(state: ErrorViewState)
}