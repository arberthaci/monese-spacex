package al.monese.spacex.ui.rocketdetails.view

/**
 * Created by Arbër Thaçi on 18-12-24.
 * Email: arberlthaci@gmail.com
 */

interface LaunchAdapterView {

    fun adapterNotifyDataSetChanged()
}