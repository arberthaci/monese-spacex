package al.monese.spacex.ui.home.presenter

import al.monese.spacex.ui.base.presenter.BasePresenter
import al.monese.spacex.ui.home.interactor.HomeMVPInteractor
import al.monese.spacex.ui.home.view.HomeMVPView
import al.monese.spacex.ui.rocketdetails.view.RocketDetailsFragment
import al.monese.spacex.ui.rockets.view.RocketsFragment
import al.monese.spacex.utils.rx.SchedulerProvider
import android.support.v4.app.Fragment
import android.view.Menu
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

class HomePresenter<V : HomeMVPView, I : HomeMVPInteractor> @Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable), HomeMVPPresenter<V, I> {

    override fun onAttach(view: V?) {
        super.onAttach(view)

        getView()?.let {
            if (interactor?.retrieveIsFirstOpenFromPrefs() == true)
                it.showWelcomeDialog()
            else
                it.openRocketsFragment()
        }
    }

    override fun onFragmentResumed() {
        when(getCurrentFragment()) {
            is RocketsFragment -> getView()?.let {
                it.hideBackButton()
                it.setToolbarTitle(RocketsFragment.TITLE)
            }
            is RocketDetailsFragment -> getView()?.let {
                it.showBackButton()
                it.setToolbarTitle(RocketDetailsFragment.TITLE)
            }
        }

        getView()?.invalidateOptionsMenuEvent()
    }

    override fun onBackPressed(): Boolean? {
        val currentFragment = getCurrentFragment()
        when (currentFragment) {
            is RocketsFragment -> return true
            is RocketDetailsFragment -> getView()?.closeRocketDetailsFragment()
        }
        return null
    }

    override fun onInvalidateOptionsMenu(menu: Menu?) {
        when(getCurrentFragment()) {
            is RocketsFragment -> getView()?.showToolbarForRockets(menu)
            is RocketDetailsFragment -> { /* do nothing */ }
        }
    }

    override fun onOptionsItemFilterSelected() {
        (getCurrentFragment() as? RocketsFragment)?.onFilterRocketsClicked()
    }

    override fun onWelcomeDialogClosed() {
        interactor?.updateIsFirstOpenInPrefs(isFirstOpen = false)
        getView()?.openRocketsFragment()
    }

    private fun getCurrentFragment(): Fragment? = getView()?.getCurrentFragment()
}