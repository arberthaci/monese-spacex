package al.monese.spacex.ui.rockets.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import al.monese.spacex.R
import al.monese.spacex.data.network.model.Rocket
import al.monese.spacex.ui.base.view.BaseActivity
import al.monese.spacex.ui.base.view.BaseFragment
import al.monese.spacex.ui.rockets.interactor.RocketsMVPInteractor
import al.monese.spacex.ui.rockets.presenter.RocketsMVPPresenter
import al.monese.spacex.utils.customviews.ErrorViewState
import al.monese.spacex.utils.extensions.hide
import al.monese.spacex.utils.extensions.show
import al.monese.spacex.utils.rvadapterlisteners.OnRocketActionListener
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.listItemsSingleChoice
import kotlinx.android.synthetic.main.fragment_rockets.*
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

class RocketsFragment : BaseFragment(), RocketsMVPView, BaseActivity.RocketsFragmentCallBack, OnRocketActionListener {

    @Inject
    internal lateinit var presenter: RocketsMVPPresenter<RocketsMVPView, RocketsMVPInteractor>
    @Inject
    internal lateinit var rocketAdapter: RocketAdapter
    @Inject
    internal lateinit var layoutManager: LinearLayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_rockets, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.onAttach(this)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        presenter.onDetach()
        super.onDestroyView()
    }

    /**
     **********************************
     * BaseFragment implementation
     * Start
     **********************************
     **/

    override fun setUp() {
        swipe_to_refresh.setColorSchemeResources(R.color.colorPrimary)
        swipe_to_refresh.setOnRefreshListener { presenter.onSwipeToRefreshOccurred() }

        layoutManager.orientation = LinearLayoutManager.VERTICAL
        rv_rockets.layoutManager = layoutManager
        rv_rockets.itemAnimator = DefaultItemAnimator()
        rv_rockets.setHasFixedSize(false)
        rv_rockets.adapter = rocketAdapter
        rocketAdapter.onRocketActionListener = this

        presenter.onViewPrepared()
    }

    /**
     **********************************
     * BaseFragment implementation
     * End
     **********************************
     **/

    /**
     **********************************
     * OnRocketActionListener implementation
     * Start
     **********************************
     **/

    override fun onItemSelected(rocket: Rocket) {
        getBaseActivity()?.redirectToRocketDetailsFragment(rocket = rocket)
    }

    /**
     **********************************
     * OnRocketActionListener implementation
     * End
     **********************************
     **/

    /**
     **********************************
     * RocketsFragmentCallBack implementation
     * Start
     **********************************
     **/

    override fun onFilterRocketsClicked() {
        presenter.onFilterRocketsSelected()
    }

    /**
     **********************************
     * RocketsFragmentCallBack implementation
     * End
     **********************************
     **/

    /**
     **********************************
     * RocketsMVPView implementation
     * Start
     **********************************
     **/

    override fun prepareLoading() {
        showLoadingIndicator()
        ev_rockets.setState(ErrorViewState.NO_ERROR)
        rv_rockets.hide()
        rocketAdapter.presenter.clearList()
    }

    override fun displayRockets(rockets: List<Rocket>?): Unit? = rockets?.let {
        rocketAdapter.presenter.insertRocketsToList(it)
        ev_rockets.setState(ErrorViewState.NO_ERROR)
        rv_rockets.show()
        swipe_to_refresh.isRefreshing = false
        dismissLoadingIndicator()
    }

    override fun showErrorView(state: ErrorViewState) {
        rv_rockets.hide()
        ev_rockets.setState(state)
        swipe_to_refresh.isRefreshing = false
        dismissLoadingIndicator()
    }

    override fun openFilterRocketsDialog() {
        MaterialDialog(context!!)
                .title(R.string.dialog_display_rockets_title)
                .listItemsSingleChoice(items = arrayListOf("All rockets", "Only active rockets", "Only inactive rockets")) { dialog, index, _ ->
                    dialog.dismiss()
                    presenter.onFilterRocketsApplied(filterIndex = index)
                }
                .positiveButton(R.string.dialog_filter_label)
                .show()
    }

    /**
     **********************************
     * RocketsMVPView implementation
     * End
     **********************************
     **/

    companion object {
        internal val TAG = "rockets_ft"
        internal val TITLE = "SpaceX Rockets"

        fun newInstance(): RocketsFragment {
            return RocketsFragment()
        }
    }
}
