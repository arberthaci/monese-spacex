package al.monese.spacex.ui.rocketdetails

import al.monese.spacex.ui.rocketdetails.view.RocketDetailsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

@Module
internal abstract class RocketDetailsFragmentProvider {

    @ContributesAndroidInjector(modules = [RocketDetailsFragmentModule::class])
    internal abstract fun provideRocketDetailsFragmentFactory(): RocketDetailsFragment
}