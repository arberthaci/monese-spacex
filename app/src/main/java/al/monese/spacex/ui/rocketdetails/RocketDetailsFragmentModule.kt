package al.monese.spacex.ui.rocketdetails

import al.monese.spacex.ui.rocketdetails.interactor.RocketDetailsInteractor
import al.monese.spacex.ui.rocketdetails.interactor.RocketDetailsMVPInteractor
import al.monese.spacex.ui.rocketdetails.presenter.LaunchAdapterPresenter
import al.monese.spacex.ui.rocketdetails.presenter.RocketDetailsMVPPresenter
import al.monese.spacex.ui.rocketdetails.presenter.RocketDetailsPresenter
import al.monese.spacex.ui.rocketdetails.view.LaunchAdapter
import al.monese.spacex.ui.rocketdetails.view.RocketDetailsFragment
import al.monese.spacex.ui.rocketdetails.view.RocketDetailsMVPView
import android.support.v7.widget.LinearLayoutManager
import dagger.Module
import dagger.Provides

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

@Module
class RocketDetailsFragmentModule {

    @Provides
    internal fun provideRocketDetailsInteractor(interactor: RocketDetailsInteractor): RocketDetailsMVPInteractor = interactor

    @Provides
    internal fun provideRocketDetailsPresenter(presenter: RocketDetailsPresenter<RocketDetailsMVPView, RocketDetailsMVPInteractor>)
            : RocketDetailsMVPPresenter<RocketDetailsMVPView, RocketDetailsMVPInteractor> = presenter

    @Provides
    internal fun provideLaunchAdapterPresenter(): LaunchAdapterPresenter = LaunchAdapterPresenter(arrayListOf())

    @Provides
    internal fun provideLaunchAdapter(presenter: LaunchAdapterPresenter): LaunchAdapter = LaunchAdapter(presenter)

    @Provides
    internal fun provideLinearLayoutManager(fragment: RocketDetailsFragment): LinearLayoutManager = LinearLayoutManager(fragment.activity)
}