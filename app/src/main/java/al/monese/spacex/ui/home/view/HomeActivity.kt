package al.monese.spacex.ui.home.view

import al.monese.spacex.R
import al.monese.spacex.data.network.model.Rocket
import al.monese.spacex.ui.base.view.BaseActivity
import al.monese.spacex.ui.home.interactor.HomeMVPInteractor
import al.monese.spacex.ui.home.presenter.HomeMVPPresenter
import al.monese.spacex.ui.rocketdetails.view.RocketDetailsFragment
import al.monese.spacex.ui.rockets.view.RocketsFragment
import al.monese.spacex.utils.AppConstants
import al.monese.spacex.utils.extensions.addFragment
import al.monese.spacex.utils.extensions.getCurrentFragment
import al.monese.spacex.utils.extensions.removeFragment
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.Menu
import android.view.MenuItem
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

class HomeActivity : BaseActivity(), HomeMVPView, HasSupportFragmentInjector {

    @Inject
    internal lateinit var presenter: HomeMVPPresenter<HomeMVPView, HomeMVPInteractor>
    @Inject
    internal lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        presenter.onAttach(this)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    override fun supportFragmentInjector() = fragmentDispatchingAndroidInjector

    override fun onBackPressed() {
        presenter.onBackPressed()?.let {
            if (it) super.onBackPressed()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        presenter.onBackPressed()?.let {
            if (it) super.onBackPressed()
        }

        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.toolbar, menu)
        hideOptionsMenu(menu)
        presenter.onInvalidateOptionsMenu(menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_filter -> { presenter.onOptionsItemFilterSelected() }
        }

        return super.onOptionsItemSelected(item)
    }

    /**
     **********************************
     * HomeMVPView implementation
     * Start
     **********************************
     **/

    override fun setToolbarTitle(title: String) {
        supportActionBar?.title = title
    }

    override fun getCurrentFragment(): Fragment? = supportFragmentManager?.getCurrentFragment(R.id.home_fragment_container)

    override fun openRocketsFragment() = openNewFragment(fragment = RocketsFragment.newInstance(),
            tag = RocketsFragment.TAG,
            title = RocketsFragment.TITLE,
            fragmentAnimationMode = AppConstants.FragmentAnimationMode.ANIMATION_FADE)

    override fun openRocketDetailsFragment(rocket: Rocket) {
        val rocketDetailsFragment = RocketDetailsFragment.newInstance()
        rocketDetailsFragment.rocket = rocket

        openNewFragment(fragment = rocketDetailsFragment,
                tag = RocketDetailsFragment.TAG,
                title = RocketDetailsFragment.TITLE,
                fragmentAnimationMode = AppConstants.FragmentAnimationMode.ANIMATION_SLIDE)
    }

    override fun closeRocketDetailsFragment() = onFragmentDetached(tag = RocketDetailsFragment.TAG, fragmentAnimationMode = AppConstants.FragmentAnimationMode.ANIMATION_SLIDE)

    override fun invalidateOptionsMenuEvent(): Unit? = invalidateOptionsMenu()

    override fun showToolbarForRockets(menu: Menu?) = menu?.let { mMenu ->
        mMenu.findItem(R.id.action_filter)?.let { it.isVisible = true }
    }

    override fun showBackButton() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun hideBackButton() {
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.setDisplayShowHomeEnabled(false)
    }

    override fun showWelcomeDialog() {
        MaterialDialog(this).show {
            title(R.string.dialog_welcome_title)
            customView(R.layout.dialog_custom_view_welcome)
            negativeButton(R.string.dialog_lets_start_label) {
                it.dismiss()
                presenter.onWelcomeDialogClosed()
            }
            noAutoDismiss()
            setCanceledOnTouchOutside(false)
        }

    }

    /**
     **********************************
     * HomeMVPView implementation
     * End
     **********************************
     **/

    /**
     **********************************
     * BaseFragment.CallBack implementation
     * Start
     **********************************
     **/

    override fun onFragmentAttached() {}

    override fun onFragmentResumed() = presenter.onFragmentResumed()

    override fun onFragmentDetached(tag: String, fragmentAnimationMode: AppConstants.FragmentAnimationMode, detachInBackground: Boolean) {
        supportFragmentManager?.removeFragment(tag = tag, fragmentAnimationMode = fragmentAnimationMode)
        if (!detachInBackground) onFragmentResumed()
    }

    override fun openNewFragment(fragment: Fragment, tag: String, title: String?, fragmentAnimationMode: AppConstants.FragmentAnimationMode) {
        supportFragmentManager.addFragment(containerViewId = R.id.home_fragment_container,
                fragment = fragment,
                tag = tag,
                fragmentAnimationMode = fragmentAnimationMode)
        title?.let { setToolbarTitle(it) }
    }

    override fun redirectToRocketDetailsFragment(rocket: Rocket) {
        openRocketDetailsFragment(rocket = rocket)
    }

    /**
     **********************************
     * BaseFragment.CallBack implementation
     * End
     **********************************
     **/

    private fun hideOptionsMenu(menu: Menu?) {
        menu?.let {
            it.findItem(R.id.action_filter)?.let { mMenu -> mMenu.isVisible = false }
        }
    }
}
