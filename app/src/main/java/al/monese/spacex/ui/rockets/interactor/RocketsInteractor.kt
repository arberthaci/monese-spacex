package al.monese.spacex.ui.rockets.interactor

import al.monese.spacex.data.db.DbOperations
import al.monese.spacex.data.network.ApiHelper
import al.monese.spacex.data.network.model.Rocket
import al.monese.spacex.data.prefs.PreferenceHelper
import al.monese.spacex.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

class RocketsInteractor @Inject internal constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper, dbOperations: DbOperations) : BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper, dbOperations = dbOperations), RocketsMVPInteractor {

    override fun retrieveRocketsFromServer(): Observable<List<Rocket>> =
            apiHelper.provideRocketsService().getAllRockets()

    override fun retrieveRocketsFromRealm(): List<Rocket> =
            dbOperations.retrieveAllRockets()

    override fun insertOrUpdateRocketsInRealm(rockets: List<Rocket>) =
            dbOperations.insertRockets(rockets = rockets)
}