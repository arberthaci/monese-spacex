package al.monese.spacex.ui.rockets.presenter

import al.monese.spacex.ui.base.presenter.MVPPresenter
import al.monese.spacex.ui.rockets.interactor.RocketsMVPInteractor
import al.monese.spacex.ui.rockets.view.RocketsMVPView

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

interface RocketsMVPPresenter <V : RocketsMVPView, I : RocketsMVPInteractor> : MVPPresenter<V, I> {

    fun onViewPrepared()

    fun onSwipeToRefreshOccurred()

    fun onFilterRocketsSelected()
    fun onFilterRocketsApplied(filterIndex: Int)
}