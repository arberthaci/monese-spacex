package al.monese.spacex.ui.rocketdetails.interactor

import al.monese.spacex.data.db.DbOperations
import al.monese.spacex.data.network.ApiHelper
import al.monese.spacex.data.network.model.Launch
import al.monese.spacex.data.prefs.PreferenceHelper
import al.monese.spacex.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

class RocketDetailsInteractor @Inject internal constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper, dbOperations: DbOperations) : BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper, dbOperations = dbOperations), RocketDetailsMVPInteractor {

    override fun retrieveLaunchesByRocketIdFromServer(rocketId: String): Observable<List<Launch>> {
        val filters: HashMap<String, String> = hashMapOf()
        filters["rocket_id"] = rocketId
        return apiHelper.provideLaunchesService().getAllLaunchesByRocketId(filterParameters = filters)
    }

    override fun retrieveLaunchesByRocketIdFromRealm(rocketId: String): List<Launch> =
            dbOperations.retrieveAllLaunchesByRocketId(rocketId = rocketId)

    override fun insertOrUpdateLaunchesInRealm(launches: List<Launch>) =
            dbOperations.insertLaunches(launches = launches)
}