package al.monese.spacex.di

import javax.inject.Qualifier

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

@Qualifier
@Retention annotation class PreferenceInfo