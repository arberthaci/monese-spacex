package al.monese.spacex.di.builder

import al.monese.spacex.ui.home.HomeActivityModule
import al.monese.spacex.ui.home.view.HomeActivity
import al.monese.spacex.ui.rocketdetails.RocketDetailsFragmentProvider
import al.monese.spacex.ui.rockets.RocketsFragmentProvider
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(HomeActivityModule::class), (RocketsFragmentProvider::class), (RocketDetailsFragmentProvider::class)])

    abstract fun bindHomeActivity(): HomeActivity
}