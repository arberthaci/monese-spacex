package al.monese.spacex.di.module

import al.monese.spacex.data.db.DbMigration
import al.monese.spacex.data.db.DbOperations
import al.monese.spacex.data.db.launches.LaunchesHelper
import al.monese.spacex.data.db.launches.LaunchesRepository
import al.monese.spacex.data.db.rockets.RocketsHelper
import al.monese.spacex.data.db.rockets.RocketsRepository
import al.monese.spacex.data.network.ApiHelper
import al.monese.spacex.data.prefs.AppPreferenceHelper
import al.monese.spacex.data.prefs.PreferenceHelper
import al.monese.spacex.di.PreferenceInfo
import al.monese.spacex.utils.AppConstants
import al.monese.spacex.utils.rx.SchedulerProvider
import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import io.realm.Realm
import io.realm.RealmConfiguration
import javax.inject.Singleton

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

@Module
class AppModule {

    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context = application

    @Provides
    @Singleton
    internal fun provideRealmDatabase(context: Context): Realm {
        Realm.init(context)
        return Realm.getInstance(RealmConfiguration.Builder()
                .name("realm.monese.spacex")
                .schemaVersion(1)
                .migration(DbMigration())
                .build())
    }

    @Provides
    @PreferenceInfo
    internal fun providePrefFileName(): String = AppConstants.SHARED_PREFS_NAME

    @Provides
    @Singleton
    internal fun providePrefHelper(appPreferenceHelper: AppPreferenceHelper): PreferenceHelper = appPreferenceHelper

    @Provides
    @Singleton
    internal fun provideApiHelper(): ApiHelper = ApiHelper()

    @Provides
    @Singleton
    internal fun provideDbOperations(rocketsHelper: RocketsHelper, launchesHelper: LaunchesHelper): DbOperations =
            DbOperations(rocketsHelper = rocketsHelper, launchesHelper = launchesHelper)

    @Provides
    @Singleton
    internal fun provideRocketsHelper(realm: Realm): RocketsHelper = RocketsRepository(realm)

    @Provides
    @Singleton
    internal fun provideLaunchesHelper(realm: Realm): LaunchesHelper = LaunchesRepository(realm)

    @Provides
    internal fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    internal fun provideSchedulerProvider(): SchedulerProvider = SchedulerProvider()
}