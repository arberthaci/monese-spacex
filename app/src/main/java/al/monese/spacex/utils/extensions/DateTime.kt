package al.monese.spacex.utils.extensions

import al.monese.spacex.utils.AppConstants
import org.joda.time.DateTime

/**
 * Created by Arbër Thaçi on 18-12-24.
 * Email: arberlthaci@gmail.com
 */

internal fun DateTime.toString(type: AppConstants.DateStringMode): String? {
    val months = arrayListOf("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December")

    return when (type) {
        AppConstants.DateStringMode.DATE_STRING_MODE -> { "${this.dayOfMonth} ${months[this.monthOfYear - 1]} ${this.year}" }
        AppConstants.DateStringMode.DATE_STRING_WITHOUT_YEAR_MODE -> {
            var yearString = ""
            val currentYear = DateTime()
            if (currentYear.year != this.year)
                yearString = " ${this.year}"

            "${this.dayOfMonth} ${months[this.monthOfYear - 1]}$yearString"
        }
        AppConstants.DateStringMode.DATE_NUMERIC_MODE -> { "${this.dayOfMonth}/" + "${this.monthOfYear}".padStart(2, '0') + "/${this.year}" }
        AppConstants.DateStringMode.TIME_MODE -> { "${this.hourOfDay}".padStart(2, '0') + ":" + "${this.minuteOfHour}".padStart(2, '0') }
    }
}