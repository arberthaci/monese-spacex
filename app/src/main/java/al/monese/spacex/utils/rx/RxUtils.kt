package al.monese.spacex.utils.rx

import al.monese.spacex.utils.CommonUtils
import al.monese.spacex.utils.rx.exceptions.NoResultsException
import android.content.Context
import io.reactivex.Observable
import io.reactivex.exceptions.Exceptions

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

object RxUtils {

    fun isNetworkConnected(context: Context): Observable<Boolean> =
            Observable.just(CommonUtils.isNetworkConnected(context))

    fun <T> checkApiResponseData(apiResponse: List<T>): List<T> {
        val totalItems = apiResponse.size
        if (totalItems != 0) return apiResponse else throw Exceptions.propagate(NoResultsException())
    }
}