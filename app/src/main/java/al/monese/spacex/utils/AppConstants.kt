package al.monese.spacex.utils

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

object AppConstants {

    internal const val SHARED_PREFS_NAME = "monese_spacex_shared_prefs"

    enum class AlerterMode constructor(val type: Int) {
        ALERT_INFO(1),
        ALERT_WARNING(2),
        ALERT_SUCCESS(3),
        ALERT_FAILED(4)
    }

    enum class FragmentAnimationMode constructor(val type: Int) {
        ANIMATION_NONE(0),
        ANIMATION_FADE(1),
        ANIMATION_SLIDE(2)
    }

    enum class DateStringMode constructor(val type: Int) {
        DATE_STRING_MODE(1),
        DATE_STRING_WITHOUT_YEAR_MODE(2),
        DATE_NUMERIC_MODE(3),
        TIME_MODE(4)
    }
}