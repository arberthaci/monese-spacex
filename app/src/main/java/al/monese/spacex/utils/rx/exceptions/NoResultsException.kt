package al.monese.spacex.utils.rx.exceptions

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

class NoResultsException: NoSuchElementException()