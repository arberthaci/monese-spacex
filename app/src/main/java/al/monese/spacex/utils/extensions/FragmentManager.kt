package al.monese.spacex.utils.extensions

import al.monese.spacex.R
import al.monese.spacex.utils.AppConstants
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

internal fun FragmentManager.addFragment(containerViewId: Int,
                                         fragment: Fragment,
                                         tag: String,
                                         fragmentAnimationMode: AppConstants.FragmentAnimationMode = AppConstants.FragmentAnimationMode.ANIMATION_NONE) {

    val fragmentTransaction = this.beginTransaction().disallowAddToBackStack()
    when (fragmentAnimationMode) {
        AppConstants.FragmentAnimationMode.ANIMATION_FADE -> fragmentTransaction.setCustomAnimations(R.anim.abc_fade_in, R.anim.abc_fade_out)
        AppConstants.FragmentAnimationMode.ANIMATION_SLIDE -> fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
        else -> { /* don't apply animation */ }
    }
    fragmentTransaction.add(containerViewId, fragment, tag).commit()
}

internal fun FragmentManager.getCurrentFragment(containerViewId: Int): Fragment? {
    return this.findFragmentById(containerViewId)
}

internal fun FragmentManager.removeFragment(tag: String,
                                            fragmentAnimationMode: AppConstants.FragmentAnimationMode = AppConstants.FragmentAnimationMode.ANIMATION_NONE) {

    val fragmentTransaction = this.beginTransaction().disallowAddToBackStack()
    when (fragmentAnimationMode) {
        AppConstants.FragmentAnimationMode.ANIMATION_FADE -> fragmentTransaction.setCustomAnimations(R.anim.abc_fade_in, R.anim.abc_fade_out)
        AppConstants.FragmentAnimationMode.ANIMATION_SLIDE -> fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right)
        else -> { /* don't apply animation */ }
    }
    this.findFragmentByTag(tag)?.let { fragmentTransaction.remove(it).commitNow() }
}