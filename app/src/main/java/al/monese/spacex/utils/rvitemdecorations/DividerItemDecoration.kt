package al.monese.spacex.utils.rvitemdecorations

import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView

/**
 * Created by Arbër Thaçi on 18-12-24.
 * Email: arberlthaci@gmail.com
 */

class DividerItemDecoration(private val divider: Drawable): RecyclerView.ItemDecoration() {

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        super.onDraw(c, parent, state)

        parent.let {
            val dividerLeft = it.paddingLeft
            val dividerRight = it.width - it.paddingRight

            val childCount = it.childCount
            for (i in 0..childCount-2) {
                val child = it.getChildAt(i)

                val params = child.layoutParams as RecyclerView.LayoutParams

                val dividerTop = child.bottom + params.bottomMargin
                val dividerBottom = dividerTop + divider.intrinsicHeight

                divider.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom)
                divider.draw(c)
            }
        }
    }
}