package al.monese.spacex.utils

import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import java.text.DecimalFormat

/**
 * Created by Arbër Thaçi on 18-12-24.
 * Email: arberlthaci@gmail.com
 */

class YearValueFormatter : IAxisValueFormatter {

    private val mFormat: DecimalFormat = DecimalFormat("####")

    override fun getFormattedValue(value: Float, axis: AxisBase?): String {
        return mFormat.format(value)
    }
}