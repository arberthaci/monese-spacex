package al.monese.spacex.utils.extensions

import al.monese.spacex.R
import android.widget.ImageView
import com.squareup.picasso.Picasso

/**
 * Created by Arbër Thaçi on 18-12-24.
 * Email: arberlthaci@gmail.com
 */

internal fun ImageView.loadImage(url: String?, resize: Boolean = false) {
    if (!resize)
        Picasso.with(this.context)
                .load( url ?: "")
                .placeholder(R.drawable.placeholder_loading)
                .error(R.drawable.placeholder_error)
                .into(this)
    else
        Picasso.with(this.context)
                .load( url ?: "")
                .placeholder(R.drawable.placeholder_loading)
                .error(R.drawable.placeholder_error)
                .resize(200, 200)
                .centerInside()
                .into(this)
}