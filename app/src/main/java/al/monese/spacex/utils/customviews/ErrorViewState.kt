package al.monese.spacex.utils.customviews

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

enum class ErrorViewState {
    NO_ERROR,
    NO_CONNECTION,
    NO_RESULTS,
    NO_RESPONSE
}