package al.monese.spacex.utils.rvadapterlisteners

import al.monese.spacex.data.network.model.Rocket

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

interface OnRocketActionListener {

    fun onItemSelected(rocket: Rocket)
}