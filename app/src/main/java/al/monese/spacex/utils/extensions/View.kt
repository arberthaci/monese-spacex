package al.monese.spacex.utils.extensions

import android.view.View
import androidx.core.view.isVisible

/**
 * Created by Arbër Thaçi on 18-12-23.
 * Email: arberlthaci@gmail.com
 */

internal fun View.show() {
    this.let { if(!it.isVisible) it.isVisible = true }
}

internal fun View.hide() {
    this.let { if(it.isVisible) it.isVisible = false }
}